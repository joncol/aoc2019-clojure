(ns aoc-2019.many-worlds-interpretation-test
  (:require [aoc-2019.many-worlds-interpretation :as mwi]
            [clojure.string :as str]
            [clojure.test :refer [deftest is]]))

(def rows-map1 ["#########"
                "#b.A.@.a#"
                "#########"])

(def rows-map2 ["########################"
                "#f.D.E.e.C.b.A.@.a.B.c.#"
                "######################.#"
                "#d.....................#"
                "########################"])

(def rows-map3 ["########################"
                "#...............b.C.D.f#"
                "#.######################"
                "#.....@.a.B.c.d.A.e.F.g#"
                "########################"])

(def rows-map4 ["#################"
                "#i.G..c...e..H.p#"
                "########.########"
                "#j.A..b...f..D.o#"
                "########@########"
                "#k.E..a...g..B.n#"
                "########.########"
                "#l.F..d...h..C.m#"
                "#################"])

(def rows-map5 ["#######"
                "#....@#"
                "#.###A#"
                "#.###b#"
                "#.aBCc#"
                "#######"])

(def rows-map6 ["#############"
                "#.a..@.b.A.c#"
                "#############"])

(deftest part1-test
  (is (= 8 (mwi/part1 (str/join "\n" rows-map1))))
  (is (= 86 (mwi/part1 (str/join "\n" rows-map2))))
  (is (= 132 (mwi/part1 (str/join "\n" rows-map3))))
  (is (= 136 (mwi/part1 (str/join "\n" rows-map4))))
  (is (= 19 (mwi/part1 (str/join "\n" rows-map5))))
  (is (= 12 (mwi/part1 (str/join "\n" rows-map6)))))

(def rows-map7 ["#######"
                "#a.#Cd#"
                "##...##"
                "##.@.##"
                "##...##"
                "#cB#Ab#"
                "#######"])

(def rows-map8 ["#############"
                "#g#f.D#..h#l#"
                "#F###e#E###.#"
                "#dCba...BcIJ#"
                "#####.@.#####"
                "#nK.L...G...#"
                "#M###N#H###.#"
                "#o#m..#i#jk.#"
                "#############"])


(deftest quadruple-robots-test
  (is (= ["#######"
          "#a.#Cd#"
          "##@#@##"
          "#######"
          "##@#@##"
          "#cB#Ab#"
          "#######"]
         (-> rows-map7
             mwi/rows->wall-map
             mwi/quadruple-robots
             mwi/draw-map
             with-out-str
             str/split-lines))))

(deftest split-map-test
  (let [map-parts (-> rows-map7
                      mwi/rows->wall-map
                      mwi/split-map)]
    (is (= [["###"
             "#a."
             "##."]
            ["###"
             "Cd#"
             ".##"]
            [".##"
             "Ab#"
             "###"]
            ["##."
             "#cB"
             "###"]]
           (map #(-> %
                     mwi/draw-map
                     with-out-str
                     str/split-lines)
                map-parts)))))

(deftest part2-test
  (is (= 8 (mwi/part2 (str/join "\n" rows-map7))))
  #_(is (= 72 (mwi/part2 (str/join "\n" rows-map8))))
  (is (= 70 (mwi/part2 (str/join "\n" rows-map8)))))
