(ns aoc-2019.monitoring-station-test
  (:require [aoc-2019.monitoring-station :as ms]
            [clojure.string :as str]
            [clojure.test :refer [deftest is testing]]))


(def map1
  [".#..#"
   "....."
   "#####"
   "....#"
   "...##"])

(def map2
  ["......#.#."
   "#..#.#...."
   "..#######."
   ".#.#.###.."
   ".#..#....."
   "..#....#.#"
   "#..#....#."
   ".##.#..###"
   "##...#..#."
   ".#....####"])

(def map3
  [".#....#####...#.."
   "##...##.#####..##"
   "##...#...#.#####."
   "..#.....#...###.."
   "..#.#.....#....##"])

(def map4
  [".#..##.###...#######"
   "##.############..##."
   ".#.######.########.#"
   ".###.#######.####.#."
   "#####.##.#.##.###.##"
   "..#####..#.#########"
   "####################"
   "#.####....###.#.#.##"
   "##.#################"
   "#####.##.###..####.."
   "..######..##.#######"
   "####.##.####...##..#"
   ".#####..#.######.###"
   "##...#.##########..."
   "#.##########.#######"
   ".####.#.###.###.#.##"
   "....##.##.###..#####"
   ".#.#.###########.###"
   "#.#.#.#####.####.###"
   "###.##.####.##.#..##"])

(deftest parse-map-test
  (testing "parses an asteroid map"
    (is (= [[1 0] [4 0]
            [0 2] [1 2] [2 2] [3 2] [4 2]
            [4 3]
            [3 4] [4 4]]
           (ms/parse-map (str/join "\n" map1))))))

(deftest in-line-of-sight-count-test
  (testing "returns the number of other asteroids in line sight from position"
    (let [asteroids (ms/parse-map (str/join "\n" map1))]
      (is (= 7 (ms/in-line-of-sight-count asteroids [1 0])))
      (is (= 6 (ms/in-line-of-sight-count asteroids [0 2])))
      (is (= 8 (ms/in-line-of-sight-count asteroids [3 4]))))))

(deftest part1-test
  (testing "returns the max number of other visible asteroids"
    (is (= 8 (ms/part1 (str/join "\n" map1))))
    (is (= 33 (ms/part1 (str/join "\n" map2))))))

(deftest asteroid-angles-test
  (testing "orders asteroids by clockwise angles from positive y axis"
    (let [asteroids (ms/parse-map (str/join "\n" map3))]
      (ms/asteroid-angles asteroids [8 3]))))

(deftest asteroids-order-test
  (let [asteroids (ms/parse-map (str/join "\n" map3))
        asteroids-ordered (vals (ms/asteroid-angles asteroids [8 3]))]
    (testing "returns the asteroids in the desired order"
      (is (= [[8 1] [9 0] [9 1] [10 0] [9 2] [11 1] [12 1] [11 2] [15 1]]
             (take 9 (ms/asteroids-order asteroids-ordered))))))
  (let [asteroids (ms/parse-map (str/join "\n" map4))
        asteroids-ordered (vals (ms/asteroid-angles asteroids [11 13]))]
    (testing "return the correct asteroid for the 200:th to be vaporized"
      (is (= [8 2] (nth (ms/asteroids-order asteroids-ordered) (dec 200)))))))
