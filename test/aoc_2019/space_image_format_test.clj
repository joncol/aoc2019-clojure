(ns aoc-2019.space-image-format-test
  (:require [aoc-2019.space-image-format :as sif]
            [clojure.test :refer [deftest is]]))

(def image "123456789012")

(deftest layer-test
  (is (= "123456" (sif/layer image 3 2 0)))
  (is (= "789012" (sif/layer image 3 2 1))))

(deftest part2-test
  (println (sif/part2 "0222112222120000")))
