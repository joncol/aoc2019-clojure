(ns aoc-2019.universal-orbit-map-test
  (:require [aoc-2019.universal-orbit-map :as uom]
            [clojure.test :refer [deftest is testing]]))

(def objects1 ["COM)B"
               "B)C"
               "C)D"
               "D)E"
               "E)F"
               "B)G"
               "G)H"
               "D)I"
               "E)J"
               "J)K"
               "K)L"])

(deftest parse-object-map-test
  (testing "parses a list of orbits into a map"
    (is (= {:b :com
            :c :b
            :d :c
            :e :d
            :f :e
            :g :b
            :h :g
            :i :d
            :j :e
            :k :j
            :l :k} (uom/parse-object-map objects1)))))

(deftest orbit-count-test
  (let [object-map (uom/parse-object-map objects1)]
    (testing "the center of mass has no orbits"
      (is (zero? (uom/orbit-count object-map :com))))
    (testing "object D has 3 orbits"
      (is (= 3 (uom/orbit-count object-map :d))))
    (testing "object L has 3 orbits"
      (is (= 7 (uom/orbit-count object-map :l))))
    (testing "sum of all orbits is 42"
      (is (= 42 (reduce + (map (partial uom/orbit-count object-map)
                               (keys object-map))))))))

(def objects2 ["COM)B"
               "B)C"
               "C)D"
               "D)E"
               "E)F"
               "B)G"
               "G)H"
               "D)I"
               "E)J"
               "J)K"
               "K)L"
               "K)YOU"
               "I)SAN"])

(deftest distance-test
  (let [object-map (uom/parse-object-map objects2)]
    (is (= 4 (uom/distance object-map :you :san)))))
