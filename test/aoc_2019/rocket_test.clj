(ns aoc-2019.rocket-test
  (:require [aoc-2019.rocket :as rocket]
            [clojure.test :refer [deftest is testing]]))

(deftest fuel2-test1
  (testing "returns the correct fuel"
    (is (= 966 (rocket/fuel2 1969)))))

(deftest fuel2-test2
  (testing "returns the correct fuel"
    (is (= 50346 (rocket/fuel2 100756)))))
