(ns aoc-2019.secure-container-test
  (:require [aoc-2019.secure-container :as sc]
            [clojure.test :refer [deftest is]]))

(deftest valid?-test
  (is (sc/valid? "111111"))
  (is (not (sc/valid? "223450")))
  (is (not (sc/valid? "123789"))))

(deftest valid*?-test
  (is (sc/valid*? "112233"))
  (is (not (sc/valid*? "123444")))
  (is (sc/valid*? "111122")))
