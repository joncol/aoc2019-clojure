(ns aoc-2019.set-and-forget-test
  (:require [aoc-2019.set-and-forget :as saf]
            [clojure.test :refer [deftest is]]))

(def map-rows ["..#.........."
               "..#.........."
               "#######...###"
               "#.#...#...#.#"
               "#############"
               "..#...#...#.."
               "..#####...^.."])

(deftest alignment-sum-test
  (let [wall-map (saf/rows->wall-map map-rows)]
    (is (= 76 (saf/alignment-sum wall-map)))))

(deftest forward-dirs-test
  (is (= [[:forward [0 -1]] [:left [-1 0]] [:right [1 0]]]
         (saf/forward-dirs [:forward [0 -1]]))) ; up
  (is (= [[:forward [1 0]] [:left [0 -1]] [:right [0 1]]]
         (saf/forward-dirs [:forward [1 0]]))) ; right
  (is (= [[:forward [0 1]] [:left [1 0]] [:right [-1 0]]]
         (saf/forward-dirs [:forward [0 1]]))) ; down
  (is (= [[:forward [-1 0]] [:left [0 1]] [:right [0 -1]]]
         (saf/forward-dirs [:forward [-1 0]]))) ; left
  )

(deftest path-test
  (let [wall-map (saf/rows->wall-map map-rows)]
    (is (= "4,R,2,R,2,R,12,R,2,R,6,R,4,R,4,R,6"
           (saf/path wall-map)))))
