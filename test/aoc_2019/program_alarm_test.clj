(ns aoc-2019.program-alarm-test
  (:require [aoc-2019.program-alarm :as program-alarm]
            [clojure.test :refer [deftest is]]))

(deftest run-program!-test
  (is (= [2 0 0 0 99] (program-alarm/run-program! [1 0 0 0 99])))
  (is (= [2 3 0 6 99] (program-alarm/run-program! [2 3 0 3 99])))
  (is (= [2 4 4 5 99 9801] (program-alarm/run-program! [2 4 4 5 99 0])))
  (is (= [30 1 1 4 2 5 6 0 99]
         (program-alarm/run-program! [1 1 1 4 99 5 6 0 99]))))
