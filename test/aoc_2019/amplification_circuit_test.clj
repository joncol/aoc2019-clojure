(ns aoc-2019.amplification-circuit-test
  (:require [aoc-2019.amplification-circuit :as ac]
            [clojure.test :refer [deftest is testing]]))

(def program1 "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0")

(def program2 (str "3,23,3,24,1002,24,10,24,1002,23,-1,23,"
                   "101,5,23,23,1,24,23,23,4,23,99,0,0"))

(def program3 (str "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,"
                   "1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"))

(deftest part1-test
  (testing "returns the maximum thruster signal for program 1"
    (is (= 43210 (ac/part1 program1))))
  (testing "returns the maximum thruster signal for program 2"
    (is (= 54321 (ac/part1 program2))))
  (testing "returns the maximum thruster signal for program 3"
    (is (= 65210 (ac/part1 program3)))))

(def program4 (str "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,"
                   "27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"))

(def program5
  (str "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,"
       "-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,"
       "53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"))

(deftest part2-test
  (testing "returns the maximum thruster signal for program 4"
    (is (= 139629729 (ac/part2 program4))))
  (testing "returns the maximum thruster signal for program 5"
    (is (= 18216 (ac/part2 program5)))))
