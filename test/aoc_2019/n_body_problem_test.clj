(ns aoc-2019.n-body-problem-test
  (:require [aoc-2019.n-body-problem :as nbp]
            [clojure.string :as str]
            [clojure.test :refer [deftest is testing]]))

(def positions1 ["<x=-1, y=0, z=2>"
                 "<x=2, y=-10, z=-7>"
                 "<x=4, y=-8, z=8>"
                 "<x=3, y=5, z=-1>"])

(deftest apply-gravity-test
  (let [positions (mapv nbp/parse-vector positions1)
        velocities (vec (repeat 4 [0 0 0]))
        next-velocities (nbp/apply-gravity positions velocities)]
    (is (= [[3 -1 -1] [1 3 3] [-3 1 -3] [-1 -3 1]] next-velocities))))

(deftest run-step-test
  (let [positions (mapv nbp/parse-vector positions1)
        velocities (vec (repeat 4 [0 0 0]))
        [next-positions next-velocities] (nbp/run-step positions velocities)]
    (is (= [[2 -1 1] [3 -7 -4] [1 -7 5] [2 2 0]] next-positions))
    (is (= [[3 -1 -1] [1 3 3] [-3 1 -3] [-1 -3 1]] next-velocities))))

(deftest total-energy-test
  (let [positions (mapv nbp/parse-vector positions1)
        velocities (vec (repeat 4 [0 0 0]))
        [final-positions final-velocities] (nth (iterate #(apply nbp/run-step %)
                                                         [positions velocities])
                                                10)]
    (= 179 (nbp/total-energy final-positions final-velocities))))

(deftest part2-test
  (is (= 2772 (nbp/part2 (str/join "\n" positions1)))))
