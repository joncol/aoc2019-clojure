(ns aoc-2019.crossed-wires-test
  (:require [aoc-2019.crossed-wires :as cw]
            [clojure.test :refer [are deftest is testing]]))

(def wires1 ["R8,U5,L5,D3"
             "U7,R6,D4,L4"])

(def wires2 ["R75,D30,R83,U83,L12,D49,R71,U7,L72"
             "U62,R66,U55,R34,D71,R55,D58,R83"])

(def wires3 ["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
             "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"])

(deftest intersections-test
  (testing "finds the intersection points between two wires"
    (let [[w1 w2] (map (comp cw/segments cw/parse-wire) wires1)
          points (map :pos (cw/segment-intersections w1 w2))]
      (is (= 2 (count points)))
      (is (some #{[6 5]} points))
      (is (some #{[3 3]} points)))))

(deftest distance-to-closest-intersection-test
  (are [wires distance] (= distance (cw/distance-to-closest-intersection wires))
    wires1 6
    wires2 159
    wires3 135))

(deftest lowest-signal-delay-test
  (are [wires delay] (= delay (cw/lowest-signal-delay wires))
    wires1 30
    wires2 610
    wires3 410))
