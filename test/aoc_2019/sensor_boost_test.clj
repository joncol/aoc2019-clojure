(ns aoc-2019.sensor-boost-test
  (:require [aoc-2019.sensor-boost :as sb]
            [clojure.test :refer [deftest is testing]]))

(def program1 "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")

(def program2 "1102,34915192,34915192,7,4,7,99,0")

(def program3 "104,1125899906842624,99")

(deftest run-program-test
  (testing "returns the correct output for program 1"
    (is (= [109 1 204 -1 1001 100 1 100 1008 100 16 101 1006 101 0 99]
           (->> program1 sb/parse-program (sb/run-program 1)))))
  (testing "returns the correct output for program 2"
    (is (= 16
           (->> program2 sb/parse-program (sb/run-program 1) first str count))))
  (testing "returns the correct output for program 3"
    (is (= [1125899906842624]
           (->> program3 sb/parse-program (sb/run-program 1))))))
