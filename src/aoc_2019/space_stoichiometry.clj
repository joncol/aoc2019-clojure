(ns aoc-2019.space-stoichiometry
  (:require [clojure.string :as str]
            [instaparse.core :as insta]))

(def rule-parser
  (insta/parser
   "<rule>     = ingredients <' => '> result
   ingredients = ingredient (<', '> ingredient)*
   <result>    = ingredient
   ingredient  = number <' '> #'\\w+'
   number      = #'\\d+'"))

(defn parse-rule [s]
  (let [v (->> s
               (insta/parse rule-parser)
               (insta/transform {:ingredient vector
                                 :ingredients vector
                                 :number #(Integer/parseUnsignedInt %)}))
        ingredient (second (second v))
        quantity (first (second v))]
    [ingredient [quantity (first v)]]))

(defn parse-rules [data]
  (->> data
       str/split-lines
       (map parse-rule)
       (into {})))

(defn add-to-leftovers [leftovers quantity item]
  (update leftovers item (fnil #(+ % quantity) 0)))

(defn take-from-leftovers [leftovers quantity item]
  (update leftovers item #(- % quantity)))

(defn lookup-immediate-ingredients [rules leftovers quantity item]
  (let [[m ingredients] (get rules item)
        mult (int (Math/ceil (/ quantity m)))
        surplus (- (* mult m) quantity)]
    [(mapv (fn [x] (-> [(* mult (first x)) (second x)]))
           ingredients)
     (if (pos? surplus)
       (add-to-leftovers leftovers surplus item)
       leftovers)]))

(defn create-ingredient [rules leftovers ^Integer quantity item]
  (if (= "ORE" item)
    [[[quantity item]] leftovers]
    (cond
      (< quantity (get leftovers item 0))
      [[] (take-from-leftovers leftovers quantity item)]

      (= quantity (get leftovers item))
      [[] (dissoc leftovers item)]

      :else
      (let [quantity* (- quantity (get leftovers item 0))]
        (lookup-immediate-ingredients rules
                                      (dissoc leftovers item)
                                      quantity*
                                      item)))))

(defn ore-requirements [fuel-count leftovers rules]
  (loop [[items leftovers] [[[fuel-count "FUEL"]] leftovers]]
    (let [[ores [item & items*]] (split-with #(= "ORE" (second %)) items)]
      (if item
        (let [[item-reqs leftovers*] (apply create-ingredient rules leftovers
                                            item)]
          (recur [(concat ores item-reqs items*) leftovers*]))
        [(reduce + (map first items)) leftovers]))))

(def part1 (comp first (partial ore-requirements 1 {}) parse-rules))

(defn multiply-leftovers [leftovers m]
  (->> leftovers
       (map (fn [[k v]] [k (*' m v)]))
       (into {})))

(defn first-repeated-element-indices [xs]
  (first (filter #(number? (first %))
                 (reductions (fn [[m i] x]
                               (if-let [xi (get m x)]
                                 [xi i]
                                 [(assoc m x i) (inc i)]))
                             [(hash-map) 0]
                             xs))))

#_(defn part2 [data]
    (let [rules (parse-rules data)]
      (->> (iterate (fn [[_ leftovers]]
                      (ore-requirements 100 leftovers rules))
                    [0 {}])
           (map first)
           (drop 1)
           (reductions +)
           ;; (take-while #(<= % 1000000000000))
           (take-while    #(<= % 1000000000000))
           count
           (* 100)
           #_(str/join "\n")
           #_first-repeated-element-indices)))

#_(defn part2 [data]
    (let [rules (parse-rules data)]
      (loop [fuel-count 0
             total-ore-count 0
             ore-left 1000000000000
             leftovers {}
             i 0]
        (let [[ore-count leftovers] (ore-requirements 1 leftovers rules)
              ore-left (-' ore-left ore-count)]
          ;; (println)
          ;; (println (inc fuel-count) ore-count)
          ;; (clojure.pprint/pprint (into (sorted-map) leftovers))
          (when (empty? leftovers)
            (println "repeat found:" (inc fuel-count) total-ore-count leftovers))
          (if (and (< i 10000)
                   (not (neg? ore-left)))
            (recur (inc fuel-count)
                   (+ ore-count total-ore-count)
                   ore-left
                   leftovers
                   (inc i))
            [fuel-count leftovers])))))

(defn part2 [data]
  (let [total-ore-count 1000000000000
        step-size 2000 ;; Can't really explain why this works
        rules (parse-rules data)
        [ore-for-step leftovers] (ore-requirements step-size {} rules)
        n (quot total-ore-count ore-for-step)]
    ;; (println "ore 1st step fuel:" ore-for-step)
    ;; (println "n:" n)
    ;; (println "leftovers:    " leftovers)
    ;; (println "mul leftovers:" (multiply-leftovers leftovers n))
    (loop [fuel-count (* step-size n)
           ore-left (- total-ore-count (* n ore-for-step))
           leftovers (multiply-leftovers leftovers n)
           i 0]
      ;; (println "ore-left:" ore-left)
      (let [[ore-req leftovers*] (ore-requirements 1 leftovers rules)
            ore-left (- ore-left ore-req)]
        ;; (println (format "fuel-count: %d (%d)" fuel-count i))
        ;; (println "ore-req:" ore-req)
        (if (not (neg? ore-left))
          (if (< i 300000)
            (recur (inc fuel-count) ore-left leftovers* (inc i))
            (println (format "STOPPING; fuel-count: %d, ore-left: %d, req: %d, leftovers: %s" fuel-count (biginteger ore-left) ore-req leftovers)))
          [fuel-count leftovers])))))
