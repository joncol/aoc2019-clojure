(ns aoc-2019.many-worlds-interpretation
  (:require [clojure.core.cache.wrapped :as c]
            [clojure.string :as str]))

(defn rows->wall-map [rows]
  (->> rows
       (map-indexed (fn [yi row]
                      (map-indexed (fn [xi at-pos]
                                     [[xi yi] at-pos])
                                   row)))
       (mapcat vec)
       (into {})))

(defn find-pos [wall-map x]
  (cond
    (fn? x) (->> wall-map
                 (filter (fn [[_ v]] (x v)))
                 (into {}))
    (char? x) (->> wall-map
                   (filter (fn [[_ v]] (= x v)))
                   ffirst)))

(defn connected-neighbors [wall-map [[x y] door-keys]]
  (->> [[x (inc y)]
        [x (dec y)]
        [(dec x) y]
        [(inc x) y]]
       (keep (fn [pos]
               (let [at-pos (get wall-map pos)]
                 (cond
                   (nil? at-pos)
                   nil

                   (or (= \. at-pos) (= \@ at-pos))
                   [pos door-keys]

                   (Character/isLowerCase at-pos)
                   [pos (conj door-keys at-pos)]

                   (Character/isUpperCase at-pos)
                   (let [k (Character/toLowerCase at-pos)]
                     (when (or (door-keys k)
                               (not (find-pos wall-map k)))
                       [pos door-keys]))

                   :else nil))))))

(defn find-shortest-path [wall-map]
  (let [start-pos (find-pos wall-map \@)
        distances (c/lru-cache-factory {[start-pos #{}] 0}
                                       :threshold (int (Math/pow 2 15)))
        total-key-count (count (find-pos wall-map #(Character/isLowerCase %)))]
    (loop [q (conj clojure.lang.PersistentQueue/EMPTY [0 [start-pos #{}]])]
      (let [[dist node] (peek q)]
        (if (= (count (second node)) total-key-count)
          dist
          (let [not-visited (->> (connected-neighbors wall-map node)
                                 (remove #(c/has? distances %)))]
            (doseq [n not-visited]
              (c/miss distances n (inc dist)))
            (recur (apply conj (pop q) (map (fn [n] [(inc dist) n])
                                            not-visited)))))))))

(defn part1 [data]
  (let [wall-map (rows->wall-map (str/split-lines data))]
    (find-shortest-path wall-map)))

(defn quadruple-robots [wall-map]
  (let [[sx sy] (find-pos wall-map \@)]
    (assoc wall-map
           [sx sy] \#
           [sx (inc sy)] \#
           [(inc sx) sy] \#
           [sx (dec sy)] \#
           [(dec sx) sy] \#
           [(inc sx) (inc sy)] \@
           [(inc sx) (dec sy)] \@
           [(dec sx) (dec sy)] \@
           [(dec sx) (inc sy)] \@)))

(defn draw-map [wall-map]
  (let [points (keys wall-map)
        x-min (first (apply min-key first points))
        x-max (first (apply max-key first points))
        y-min (second (apply min-key second points))
        y-max (second (apply max-key second points))]
    (doseq [y (range y-min (inc y-max))]
      (doseq [x (range x-min (inc x-max))]
        (print (get wall-map [x y])))
      (println))))

(defn split-map [wall-map]
  (let [w (inc (apply max (map first (keys wall-map))))
        h (inc (apply max (map second (keys wall-map))))]
    [(->> wall-map
          (filter (fn [[[x y] _]] (and (< x (quot w 2))
                                       (< y (quot h 2)))))
          (into {}))
     (->> wall-map
          (filter (fn [[[x y] _]] (and (> x (quot w 2))
                                       (< y (quot h 2)))))
          (into {}))
     (->> wall-map
          (filter (fn [[[x y] _]] (and (> x (quot w 2))
                                       (> y (quot h 2)))))
          (into {}))
     (->> wall-map
          (filter (fn [[[x y] _]] (and (< x (quot w 2))
                                       (> y (quot h 2)))))
          (into {}))]))

(defn part2 [data]
  (let [wall-map (quadruple-robots (rows->wall-map (str/split-lines data)))
        map-parts (split-map wall-map)]
    (reduce + (map find-shortest-path map-parts))))
