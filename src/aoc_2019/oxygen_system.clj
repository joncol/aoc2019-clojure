(ns aoc-2019.oxygen-system
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(def wall-map (atom {[0 0] 0}))

(def droid-pos (atom [0 0]))

(def droid-path (atom []))

(def backtracking (atom false))

(def oxygen-system-pos (atom nil))

(def step-count (atom 0))

(def shortest-path-length (atom 0))

(defn move-dir [[x y] dir]
  (case dir
    1 [x (inc y)]
    2 [x (dec y)]
    3 [(dec x) y]
    4 [(inc x) y]))

(defn- opposite-dir [dir]
  (case dir
    1 2
    2 1
    3 4
    4 3
    nil))

(defn- next-unexplored-dir [wall-map pos last-dir]
  (->> (for [dir (range 1 5) :when (not= dir (opposite-dir last-dir))]
         (let [new-pos (move-dir pos dir)]
           [dir (get wall-map new-pos)]))
       (filter #(nil? (second %)))
       ffirst))

(defn run-program [program]
  (loop [memory program
         ip 0
         rel-base 0]
    (let [op (mod (get memory ip) 100)
          [x y] (params memory ip rel-base)
          dest (delay (dest memory ip rel-base))]
      (case op
        (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                (recur (assoc memory @dest (op-fn x y)) (+ ip 4) rel-base))
        3 (let [dir (next-unexplored-dir @wall-map @droid-pos (last @droid-path))
                dir (if (nil? dir)
                      (do
                        (reset! backtracking true)
                        (when (seq @droid-path)
                          (let [opp-dir (opposite-dir (last @droid-path))]
                            (swap! droid-path pop)
                            (swap! droid-pos #(move-dir % opp-dir))
                            (swap! step-count dec)
                            opp-dir)))
                      (do
                        (reset! backtracking false)
                        dir))]
            (when-not @backtracking
              (swap! droid-path #(conj % dir)))
            (recur (assoc memory @dest dir) (+ ip 2) rel-base))
        4 (do (case x
                0 (do
                    (swap! wall-map #(assoc % (move-dir @droid-pos (last @droid-path)) 1))
                    (swap! droid-path pop))
                1 (when-not @backtracking
                    (swap! droid-pos #(move-dir % (last @droid-path)))
                    (swap! step-count inc)
                    (swap! wall-map #(assoc % @droid-pos 0)))
                2 (do
                    (swap! droid-pos #(move-dir % (last @droid-path)))
                    (swap! step-count inc)
                    (swap! wall-map #(assoc % @droid-pos 0))
                    (reset! oxygen-system-pos @droid-pos)
                    (reset! shortest-path-length @step-count)))
              (recur memory (+ ip 2) rel-base))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base))
        7 (recur (assoc memory @dest (if (< x y) 1 0))
                 (+ ip 4)
                 rel-base)
        8 (recur (assoc memory @dest (if (= x y) 1 0))
                 (+ ip 4)
                 rel-base)
        9 (recur memory (+ ip 2) (+ rel-base x))
        99 nil))))

(defn draw-map [wall-map]
  (let [points (keys wall-map)
        x-min (first (apply min-key first points))
        x-max (first (apply max-key first points))
        y-min (second (apply min-key second points))
        y-max (second (apply max-key second points))]
    (doseq [y (range y-max (dec y-min) -1)]
      (doseq [x (range x-min (inc x-max))]
        (cond
          (= [0 0] [x y]) (print "X")
          (= @oxygen-system-pos [x y]) (print "O")
          :else (case (get wall-map [x y])
                  1 (print "#")
                  0 (print ".")
                  (print "?"))))
      (println))))

(defn part1 [data]
  (reset! wall-map {[0 0] 0})
  (reset! droid-pos [0 0])
  (reset! droid-path [])
  (reset! backtracking false)
  (reset! oxygen-system-pos nil)
  (reset! step-count 0)
  (reset! shortest-path-length 0)
  (->> data
       parse-program
       run-program)
  @shortest-path-length)

(defn empty-neighbors [wall-map [x y]]
  (->> [[x (inc y)]
        [x (dec y)]
        [(dec x) y]
        [(inc x) y]]
       (filter (fn [[x y]] (zero? (get wall-map [x y]))))))

(defn visited? [pos coll]
  (some #(= pos (first %)) coll))

(defn bfs [wall-map start-pos]
  (loop [queue (conj clojure.lang.PersistentQueue/EMPTY [start-pos 0])
         visited []]
    (if (empty? queue) visited
        (let [[pos time] (peek queue)
              neighbors (empty-neighbors wall-map pos)
              not-visited (->> neighbors
                               (filter (complement #(visited? % visited)))
                               (map (fn [n] [n (inc time)])))
              new-queue (apply conj (pop queue) not-visited)]
          (if (visited? pos visited)
            (recur new-queue visited)
            (recur new-queue (conj visited [pos time])))))))

(defn part2 [data]
  (part1 data)
  (->> (bfs @wall-map @oxygen-system-pos)
       (map second)
       (apply max)))
