(ns aoc-2019.space-police
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-big-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(defn color-panel! [robot-pos panels color]
  (swap! panels #(assoc % @robot-pos color)))

(defn turn-robot! [robot-pos robot-dir turn-dir]
  (let [moves [(fn [[x y]] [x (inc y)])
               (fn [[x y]] [(inc x) y])
               (fn [[x y]] [x (dec y)])
               (fn [[x y]] [(dec x) y])]]
    (case turn-dir
      0 (swap! robot-dir #(mod (dec %) 4))
      1 (swap! robot-dir #(mod (inc %) 4)))
    (swap! robot-pos (moves @robot-dir))))

(defn run-program! [panels program]
  (let [robot-pos (atom [0 0])
        robot-dir (atom 0)]
    (loop [memory program
           ip 0
           rel-base 0
           output-color? true]
      (let [op (mod (get memory ip) 100)
            [x y] (params memory ip rel-base)
            dest (delay (dest memory ip rel-base))]
        (case op
          (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                  (recur (assoc memory @dest (op-fn x y))
                         (+ ip 4)
                         rel-base
                         output-color?))
          3 (recur (assoc memory @dest (get @panels @robot-pos 0))
                   (+ ip 2)
                   rel-base
                   output-color?)
          4 (do (if output-color?
                  (color-panel! robot-pos panels x)
                  (turn-robot! robot-pos robot-dir x))
                (recur memory (+ ip 2) rel-base (not output-color?)))
          5 (let [ip* (if-not (zero? x) y (+ ip 3))]
              (recur memory ip* rel-base output-color?))
          6 (let [ip* (if (zero? x) y (+ ip 3))]
              (recur memory ip* rel-base output-color?))
          7 (recur (assoc memory @dest (if (< x y) 1 0))
                   (+ ip 4)
                   rel-base
                   output-color?)
          8 (recur (assoc memory @dest (if (= x y) 1 0))
                   (+ ip 4)
                   rel-base
                   output-color?)
          9 (recur memory (+ ip 2) (+ rel-base x) output-color?)
          99 nil)))))

(defn part1 [data]
  (let [program (parse-program data)
        panels (atom {})]
    (run-program! panels program)
    (count (keys @panels))))

(defn part2 [data]
  (let [program (parse-program data)
        panels (atom {[0 0] 1})]
    (run-program! panels program)
    (let [points (keys @panels)
          x-min (first (apply min-key first points))
          x-max (first (apply max-key first points))
          y-min (second (apply min-key second points))
          y-max (second (apply max-key second points))]
      (doseq [y (range y-max (dec y-min) -1)]
        (doseq [x (range x-min (inc x-max))]
          (if (= 1 (get @panels [x y]))
            (print "*")
            (print " ")))
        (println)))))
