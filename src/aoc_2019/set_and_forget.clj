(ns aoc-2019.set-and-forget
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(defn run-program [inputs program]
  (loop [memory program
         ip 0
         rel-base 0
         inputs inputs
         outputs []]
    (let [op (mod (get memory ip) 100)
          [x y] (params memory ip rel-base)
          dest (delay (dest memory ip rel-base))]
      (case op
        (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                (recur (assoc memory @dest (op-fn x y))
                       (+ ip 4)
                       rel-base
                       inputs
                       outputs))
        3 (recur (assoc memory @dest (int (first inputs)))
                 (+ ip 2)
                 rel-base
                 (rest inputs)
                 outputs)
        4 (recur memory (+ ip 2) rel-base inputs (conj outputs x))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base inputs outputs))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base inputs outputs))
        7 (recur (assoc memory @dest (if (< x y) 1 0))
                 (+ ip 4)
                 rel-base
                 inputs
                 outputs)
        8 (recur (assoc memory @dest (if (= x y) 1 0))
                 (+ ip 4)
                 rel-base
                 inputs
                 outputs)
        9 (recur memory (+ ip 2) (+ rel-base x) inputs outputs)
        99 outputs))))

(defn outputs->rows [outputs]
  (->> outputs
       (map char)
       (apply str)
       str/split-lines))

(defn rows->wall-map [rows]
  (->> rows
       (map-indexed (fn [yi row]
                      (map-indexed (fn [xi ch]
                                     [[xi yi] ch])
                                   row)))
       (mapcat vec)
       (into {})))

(defn neighbors [[x y]]
  [[(dec x) y]
   [(inc x) y]
   [x (dec y)]
   [x (inc y)]])

(defn intersection? [wall-map [pos _]]
  (every? #(= \# (get wall-map %)) (conj (neighbors pos) pos)))

(defn draw-map [wall-map]
  (let [points (keys wall-map)
        x-min (first (apply min-key first points))
        x-max (first (apply max-key first points))
        y-min (second (apply min-key second points))
        y-max (second (apply max-key second points))]
    (doseq [y (range y-min (inc y-max))]
      (doseq [x (range x-min (inc x-max))]
        (print (get wall-map [x y])))
      (println))))

(defn alignment-sum [wall-map]
  (->> wall-map
       (filter (partial intersection? wall-map))
       keys
       (map (fn [[x y]] (* x y)))
       (reduce +)))

(defn part1 [data]
  (let [outputs (->> data
                     parse-program
                     (run-program nil))
        wall-map (-> outputs outputs->rows rows->wall-map)]
    (alignment-sum wall-map)))

(defn rotate-ccw [[xd yd]]
  [yd (- xd)])

(defn rotate-cw [[xd yd]]
  [(- yd) xd])

(defn forward-dirs [[_ dir]]
  [[:forward dir] [:left (rotate-ccw dir)] [:right (rotate-cw dir)]])

(defn move [[px py] [_ [dx dy]]]
  [(+ px dx) (+ py dy)])

(defn step [wall-map pos dir]
  (let [walls (filter #(= \# (get wall-map %)) (neighbors pos))
        dirs (forward-dirs dir)]
    (->> dirs
         (map (fn [d] [(move pos d) d]))
         (filter (fn [[p _]] (some #(= % p) walls)))
         first)))

(defn add-turn-to-path [path new-dir]
  (conj path (case (first new-dir)
               :left \L
               :right \R)))

(defn path [wall-map]
  (let [start-pos (->> wall-map
                       (filter (fn [[_ v]] (= \^ v)))
                       ffirst)]
    (loop [pos start-pos
           dir [:forward [0 -1]]        ; start facing up
           segment-length 0
           path []]
      (let [[new-pos new-dir] (step wall-map pos dir)
            turn? (and new-dir (not= :forward (first new-dir)))
            path (if turn? (conj path segment-length) path)
            path (if turn? (add-turn-to-path path new-dir) path)]
        (if new-dir
          (recur new-pos new-dir (if turn? 1 (inc segment-length)) path)
          (->> segment-length
               (conj path)
               (remove (every-pred number? zero?))
               (str/join ",")))))))

(defn print-path [data]
  (let [outputs (->> data
                     parse-program
                     (run-program nil))
        wall-map (-> outputs outputs->rows rows->wall-map)]
    (println (path wall-map))))

(defn part2 [data]
  (let [program (-> data parse-program (assoc 0 2))
        video? false
        outputs (run-program (seq (str "A,B,A,C,B,C,B,C,A,C\n"
                                       "R,12,L,6,R,12\n"
                                       "L,8,L,6,L,10\n"
                                       "R,12,L,10,L,6,R,10\n"
                                       (if video? "y\n" "n\n")))
                             program)]
    (when video?
      (spit "set_and_forget_video_feed.txt"
            (->> outputs
                 butlast
                 (map char)
                 (apply str))))
    (last outputs)))
