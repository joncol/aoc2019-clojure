(ns aoc-2019.secure-container)

(defn deltas
  "Calculates deltas between digits in `number-str`."
  [number-str]
  (first (reduce (fn [[deltas last-digit] digit]
                   (let [delta (- (int digit)
                                  (int last-digit))]
                     [(conj deltas delta) digit]))
                 [[] (first number-str)] (rest number-str))))

(defn valid? [password-str]
  (let [deltas (deltas password-str)]
    (and (some zero? deltas)
         (not-any? neg? deltas))))

(defn password-count [pred-fn lower-bound upper-bound]
  (->> (range lower-bound (inc upper-bound))
       (map str)
       (filter pred-fn)
       count))

(def part1 (partial password-count valid?))

(defn valid*? [password-str]
  (let [deltas (deltas password-str)
        parts (partition-by identity deltas)
        groups (group-by first parts)
        zero-spans (get groups 0)]
    (and (some zero? deltas)
         (not-any? neg? deltas)
         (some #(= 1 (count %)) zero-spans))))

(def part2 (partial password-count valid*?))
