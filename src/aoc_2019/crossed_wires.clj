(ns aoc-2019.crossed-wires
  (:require [instaparse.core :as insta]
            [clojure.string :as str]))

(def wire-parser
  (insta/parser "<wire>  = segment (<','> segment)*
                 segment = dir length
                 dir     = #'[URDL]'
                 length  = #'\\d+'"))

(defn parse-wire [s]
  (->> s
       (insta/parse wire-parser)
       (insta/transform {:segment (fn [dir length] [dir length])
                         :dir #(-> % str/lower-case keyword)
                         :length #(Integer/parseUnsignedInt %)})))

(defn manhattan-distance [x y]
  (+ (Math/abs x) (Math/abs y)))

(defn segments
  "Returns horizontal and vertical line segments from parsed wire. The `dist`
  key of each segment keeps track of the distance from the \"central port\"
  (origin) to the starting point of the segment."
  [wire]
  (first (reduce (fn [[segments [x y] dist] [dir length]]
                   (case dir
                     :u [(update segments
                                 :vertical #(conj % {:x x
                                                     :y1 y
                                                     :y2 (+ y length)
                                                     :dist dist}))
                         [x (+ y length)] (+ dist length)]
                     :r [(update segments :horizontal #(conj % {:x1 x
                                                                :x2 (+ x length)
                                                                :y y
                                                                :dist dist}))
                         [(+ x length) y] (+ dist length)]
                     :d [(update segments :vertical #(conj % {:x x
                                                              :y1 y
                                                              :y2 (- y length)
                                                              :dist dist}))
                         [x (- y length)] (+ dist length)]
                     :l [(update segments :horizontal #(conj % {:x1 x
                                                                :x2 (- x length)
                                                                :y y
                                                                :dist dist}))
                         [(- x length) y] (+ dist length)]))
                 [{:horizontal []
                   :vertical []} [0 0] 0]
                 wire)))

(defn horizontal-vertical-intersection
  "Finds the intersection between a horizontal and a vertical segment.
  If the segments don't intersect, this function returns nil."
  [{:keys [x1 x2 y] :as s1} {:keys [x y1 y2] :as s2}]
  (when (and (or (<= x1 x x2)
                 (<= x2 x x1))
             (or (<= y1 y y2)
                 (<= y2 y y1)))
    {:pos [x y]
     :dist-sum (+ (:dist s1)
                  (:dist s2)
                  (Math/abs (- x x1))
                  (Math/abs (- y y1)))}))

(defn segment-intersections
  "Returns all intersections between two sets of segments."
  [segments1 segments2]
  (let [horizontal1 (:horizontal segments1)
        vertical1 (:vertical segments1)
        horizontal2 (:horizontal segments2)
        vertical2 (:vertical segments2)
        p1 (for [h horizontal1]
             (for [v vertical2]
               (horizontal-vertical-intersection h v)))
        p2 (for [h horizontal2]
             (for [v vertical1]
               (horizontal-vertical-intersection h v)))]
    (->> (concat p1 p2)
         (apply concat)
         (filter #(and (some? %) (not= [0 0] (:pos %)))))))

(defn wire-intersections [wires]
  (let [[s1 s2] (map (comp segments parse-wire) wires)]
    (segment-intersections s1 s2)))

(defn closest-distance [points]
  (let [dists (map (comp (partial apply manhattan-distance) :pos) points)]
    (apply min dists)))

(defn distance-to-closest-intersection [wires]
  (closest-distance (wire-intersections wires)))

(defn part1 [data]
  (distance-to-closest-intersection (str/split-lines data)))

(defn closest-dist-sum [points]
  (apply min (map :dist-sum points)))

(defn lowest-signal-delay [wires]
  (closest-dist-sum (wire-intersections wires)))

(defn part2 [data]
  (lowest-signal-delay (str/split-lines data)))
