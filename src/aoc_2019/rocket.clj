(ns aoc-2019.rocket
  (:require [clojure.string :as str]))

(defn fuel [mass]
  (- (quot mass 3) 2))

(defn part1 [data]
  (transduce (comp (map #(Integer/parseInt %)) (map fuel))
             + (str/split-lines data)))

(defn fuel2 [mass]
  (transduce (take-while pos?) + (rest (iterate fuel mass))))

(defn part2 [data]
  (transduce (comp (map #(Integer/parseInt %))
                   (map fuel2))
             + (str/split-lines data)))
