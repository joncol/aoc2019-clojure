(ns aoc-2019.care-package
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]
            [lanterna.terminal :as t]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(def ball-pos (atom [0 0]))

(def paddle-pos (atom [0 0]))

(defn- draw-tile [term [x y id]]
  (if (= [-1 0] [x y])
    (do (t/move-cursor term 0 0)
        (t/put-string term (str id)))
    (do (t/move-cursor term x (+ 2 y))
        (case id
          0 (t/put-string term " ")
          1 (t/put-string term "W")
          2 (t/put-string term "x")
          3 (do
              (apply t/move-cursor term @paddle-pos)
              (t/put-string term " ")
              (reset! paddle-pos [x (+ 2 y)])
              (t/move-cursor term x (+ 2 y))
              (t/put-string term "_"))
          4 (do
              (apply t/move-cursor term @ball-pos)
              (t/put-string term " ")
              (reset! ball-pos [x (+ 2 y)])
              (t/move-cursor term x (+ 2 y))
              (t/put-string term "o"))
          nil))))

(defn- simulate-input []
  ;; (Thread/sleep 50)
  (cond
    (< (first @ball-pos) (first @paddle-pos)) -1
    (< (first @paddle-pos) (first @ball-pos)) 1
    :else 0))

(defn run-program [term program]
  (loop [memory program
         ip 0
         rel-base 0
         outputs []
         tile []]
    (let [op (mod (get memory ip) 100)
          [x y] (params memory ip rel-base)
          dest (delay (dest memory ip rel-base))]
      (case op
        (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                (recur (assoc memory @dest (op-fn x y))
                       (+ ip 4)
                       rel-base
                       outputs
                       tile))
        3 (when term
            (recur (assoc memory @dest (simulate-input))
                   (+ ip 2)
                   rel-base
                   outputs
                   tile))
        4 (if (= 2 (count tile))
            (do (when term (draw-tile term (conj tile x)))
                (recur memory (+ ip 2) rel-base (conj outputs x) []))
            (recur memory (+ ip 2) rel-base (conj outputs x) (conj tile x)))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base outputs tile))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base outputs tile))
        7 (recur (assoc memory @dest (if (< x y) 1 0))
                 (+ ip 4)
                 rel-base
                 outputs
                 tile)
        8 (recur (assoc memory @dest (if (= x y) 1 0))
                 (+ ip 4)
                 rel-base
                 outputs
                 tile)
        9 (recur memory (+ ip 2) (+ rel-base x) outputs tile)
        99 outputs))))

(defn part1 [data]
  (->> data
       parse-program
       (run-program nil)
       (partition 3)
       (filter #(= 2 (nth % 2)))
       count))

(defn part2 [data]
  (let [term (t/get-terminal :swing)
        program (-> data
                    parse-program
                    (assoc 0 2))]
    (t/in-terminal term
      (t/clear term)
      (run-program term program)
      (t/get-key-blocking term {:interval 100 :timeout 5000})
      nil)))
