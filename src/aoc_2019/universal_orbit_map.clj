(ns aoc-2019.universal-orbit-map
  (:require [clojure.string :as str]))

(defn parse-object-map [lines]
  (->> lines
       (map (fn [l]
              (let [[x y] (str/split l #"\)")
                    keywordize (comp keyword str/lower-case)]
                [(keywordize  y) (keywordize  x)])))
       (into {})))

(def orbit-count
  "Gets the number of orbits from `from-object`."
  (memoize
   (fn [object-map from-object]
     (if-let [next-object (get object-map from-object)]
       (inc (orbit-count object-map next-object))
       0))))

(defn part1 [data]
  (let [object-map (parse-object-map (str/split-lines data))]
    (reduce + (map (partial orbit-count object-map) (keys object-map)))))

(defn lcp
  "Longest common prefix."
  [[x & xs] [y & ys]]
  (when (= x y)
    (cons x (lcp xs ys))))

(defn distance [object-map obj1 obj2]
  (let [path-fn #(take-while some? (iterate (partial get object-map) %))
        path1 (path-fn obj1)
        path2 (path-fn obj2)
        common-count (count (lcp (reverse path1) (reverse path2)))]
    (+ (- (dec (count path1)) common-count)
       (- (dec (count path2)) common-count))))

(defn part2 [data]
  (let [object-map (parse-object-map (str/split-lines data))]
    (distance object-map :you :san)))
