(ns aoc-2019.program-alarm
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-data [data]
  (mapv util/parse-int (str/split data #",")))

(defn run-program! [ns]
  (let [ops {1 +, 2 *}]
    (loop [ns ns
           ip 0]
      (let [op (nth ns ip)]
        (case op
          (1 2) (let [xi (nth ns (inc ip))
                      yi (nth ns (+ ip 2))
                      dest (nth ns (+ ip 3))
                      op-fun (get ops op)
                      val (op-fun (nth ns xi) (nth ns yi))]
                  (recur (assoc ns dest val) (+ ip 4)))
          99 ns)))))

(defn part1 [data]
  (-> data
      parse-data
      (assoc 1 12, 2 2)
      run-program!
      first))

(defn part2 [data]
  (let [noun-verbs (apply concat (for [noun (range 100)]
                                   (for [verb (range 100)]
                                     [noun verb])))
        results (map (fn [[noun verb]]
                       (let [result (-> data
                                        parse-data
                                        (assoc 1 noun, 2 verb)
                                        run-program!
                                        first)]
                         [result noun verb])) noun-verbs)
        [_ noun verb] (->> results
                           (filter (fn [[result _ _]] (= 19690720 result)))
                           first)]
    (+ (* 100 noun) verb)))
