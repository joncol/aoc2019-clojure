(ns aoc-2019.sensor-boost
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-big-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(defn run-program [input-val program]
  (loop [memory program
         ip 0
         rel-base 0
         outputs []]
    (let [op (mod (get memory ip) 100)
          [x y] (params memory ip rel-base)
          dest (delay (dest memory ip rel-base))]
      (case op
        (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                (recur (assoc memory @dest (op-fn x y))
                       (+ ip 4)
                       rel-base
                       outputs))
        3 (recur (assoc memory @dest input-val)
                 (+ ip 2)
                 rel-base
                 outputs)
        4 (recur memory (+ ip 2) rel-base (conj outputs x))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base outputs))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base outputs))
        7 (recur (assoc memory @dest (if (< x y) 1 0))
                 (+ ip 4)
                 rel-base
                 outputs)
        8 (recur (assoc memory @dest (if (= x y) 1 0))
                 (+ ip 4)
                 rel-base
                 outputs)
        9 (recur memory (+ ip 2) (+ rel-base x) outputs)
        99 outputs))))

(def part1 (comp str first (partial run-program 1) parse-program))

(def part2 (comp str first (partial run-program 2) parse-program))
