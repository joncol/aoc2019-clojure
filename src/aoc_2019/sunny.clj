(ns aoc-2019.sunny
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-data [data]
  (mapv util/parse-int (str/split data #",")))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr mode]
  (case mode
    0 (get memory (get memory ptr))
    1 (get memory ptr)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip]
  (let [at-ip (nth memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) (mod param-modes 10))
     (param memory (+ ip 2) (quot param-modes 10))]))

(defn run-program! [memory input]
  (let [ops {1 +
             2 *}]
    (loop [memory memory
           ip 0
           outputs []]
      (let [at-ip (nth memory ip)
            op (mod at-ip 100)
            [x y] (params memory ip)]
        (case (mod op 100)
          (1 2) (let [dest (nth memory (+ ip 3))
                      op-fn (get ops op)
                      val (op-fn x y)]
                  (recur (assoc memory dest val) (+ ip 4) outputs))
          3 (let [dest (nth memory (inc ip))]
              (recur (assoc memory dest input) (+ ip 2) outputs))
          4 (recur memory (+ ip 2) (conj outputs x))
          5 (let [ip* (if-not (zero? x) y (+ ip 3))]
              (recur memory ip* outputs))
          6 (let [ip* (if (zero? x) y (+ ip 3))]
              (recur memory ip* outputs))
          7 (let [dest (nth memory (+ ip 3))
                  val (if (< x y) 1 0)]
              (recur (assoc memory dest val) (+ ip 4) outputs))
          8 (let [dest (nth memory (+ ip 3))
                  val (if (= x y) 1 0)]
              (recur (assoc memory dest val) (+ ip 4) outputs))
          99 (->> outputs (remove zero?) first))))))

(defn part1 [data]
  (-> data
      parse-data
      (run-program! 1)))

(defn part2 [data]
  (-> data
      parse-data
      (run-program! 5)))
