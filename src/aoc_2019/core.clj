(ns aoc-2019.core
  (:gen-class)
  (:require [aoc-2019.amplification-circuit :as amplification-circuit]
            [aoc-2019.care-package :as care-package]
            [aoc-2019.crossed-wires :as crossed-wires]
            [aoc-2019.fft :as fft]
            [aoc-2019.many-worlds-interpretation :as many-worlds-interpretation]
            [aoc-2019.monitoring-station :as monitoring-station]
            [aoc-2019.n-body-problem :as n-body-problem]
            [aoc-2019.oxygen-system :as oxygen-system]
            [aoc-2019.program-alarm :as program-alarm]
            [aoc-2019.rocket :as rocket]
            [aoc-2019.secure-container :as secure-container]
            [aoc-2019.sensor-boost :as sensor-boost]
            [aoc-2019.set-and-forget :as set-and-forget]
            [aoc-2019.space-image-format :as space-image-format]
            [aoc-2019.space-police :as space-police]
            [aoc-2019.space-stoichiometry :as space-stoichiometry]
            [aoc-2019.sunny :as sunny]
            [aoc-2019.tractor-beam :as tractor-beam]
            [aoc-2019.universal-orbit-map :as universal-object-map]
            [aoc-2019.util :as util]
            [clojure.java.io :as io]))

(defn solvers []
  (into (sorted-map) {"01a" rocket/part1
                      "01b" rocket/part2
                      "02a" program-alarm/part1
                      "02b" program-alarm/part2
                      "03a" crossed-wires/part1
                      "03b" crossed-wires/part2
                      "04a" #(secure-container/part1 109165 576723)
                      "04b" #(secure-container/part2 109165 576723)
                      "05a" sunny/part1
                      "05b" sunny/part2
                      "06a" universal-object-map/part1
                      "06b" universal-object-map/part2
                      "07a" amplification-circuit/part1
                      "07b" amplification-circuit/part2
                      "08a" space-image-format/part1
                      "08b" space-image-format/part2
                      "09a" sensor-boost/part1
                      "09b" sensor-boost/part2
                      "10a" monitoring-station/part1
                      "10b" monitoring-station/part2
                      "11a" space-police/part1
                      "11b" space-police/part2
                      "12a" n-body-problem/part1
                      "12b" n-body-problem/part2
                      "13a" care-package/part1
                      "13b" care-package/part2
                      "14a" space-stoichiometry/part1
                      "14b" space-stoichiometry/part2
                      "15a" oxygen-system/part1
                      "15b" oxygen-system/part2
                      "16a" fft/part1
                      "16b" fft/part2
                      "17a" set-and-forget/part1
                      "17b" set-and-forget/part2
                      "18a" many-worlds-interpretation/part1
                      "18b" many-worlds-interpretation/part2
                      "19a" tractor-beam/part1
                      "19b" tractor-beam/part2}))

(defn run [problem-name]
  (let [day (util/parse-int problem-name)
        filename (format "resources/input%02d.txt" day)
        solver-fn (get (solvers) problem-name)]
    (println "Result:" (if (.exists (io/as-file filename))
                         (solver-fn (slurp filename))
                         (solver-fn)))))

(defn run-all []
  (doseq [problem-name (keys (solvers))]
    (print (str "Running " problem-name "..."))
    (flush)
    (time (run problem-name))
    (println)))

(defn -main []
  (run-all))
