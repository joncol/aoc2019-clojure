(ns aoc-2019.n-body-problem
  (:require [instaparse.core :as insta]
            [clojure.string :as str]
            [aoc-2019.util :as util]))

(def vector-parser
  (insta/parser "<vector> = <'<x='> number <', y='> number <', z='> number <'>'>
                 number = #'-?\\d+'"))

(defn parse-vector [s]
  (->> s
       (insta/parse vector-parser)
       (insta/transform {:number #(util/parse-int %)})
       vec))

(defn apply-gravity [positions velocities]
  (let [pairs (apply concat (for [i (range 4)]
                              (for [j (range (inc i) 4)]
                                [i j])))]
    (reduce (fn [vels [i j]]
              vels
              (let [[x1 y1 z1] (nth positions i)
                    [x2 y2 z2] (nth positions j)
                    xd (int (Math/signum (float (- x2 x1))))
                    yd (int (Math/signum (float (- y2 y1))))
                    zd (int (Math/signum (float (- z2 z1))))]
                (-> vels
                    (update i #(mapv + % [xd yd zd]))
                    (update j #(mapv - % [xd yd zd])))))
            velocities
            pairs)))

(defn apply-velocities [positions velocities]
  (mapv (fn [p v] (mapv + p v)) positions velocities))

(defn run-step [positions velocities]
  (let [next-velocities (apply-gravity positions velocities)
        next-positions (apply-velocities positions next-velocities)]
    [next-positions next-velocities]))

(defn moon-energy [[px py pz] [vx vy vz]]
  (let [pot (+ (Math/abs px)
               (Math/abs py)
               (Math/abs pz))
        kin (+ (Math/abs vx)
               (Math/abs vy)
               (Math/abs vz))]
    (* pot kin)))

(defn total-energy [positions velocities]
  (reduce + (map #(moon-energy %1 %2) positions velocities)))

(defn part1 [data]
  (let [positions (->> (str/split-lines data)
                       (mapv parse-vector))
        velocities (vec (repeat 4 [0 0 0]))
        [final-positions final-velocities] (nth (iterate #(apply run-step %)
                                                         [positions velocities])
                                                1000)]
    (total-energy final-positions final-velocities)))

(defn find-index [xs elem]
  (->> xs
       (map-indexed vector)
       (filter #(= elem (second %)))
       ffirst))

(defn gcd [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defn lcm [a b]
  (/ (* a b) (gcd a b)))

(defn lcmv [& v] (reduce lcm v))

(defn part2 [data]
  (let [positions (->> (str/split-lines data)
                       (mapv parse-vector))
        velocities (vec (repeat 4 [0 0 0]))]
    (apply lcmv (for [i (range 3)]
                  (let [xs (->> [positions velocities]
                                (iterate #(apply run-step %))
                                (map #(map (fn [xs] (nth xs i)) (first %))))]
                    (+ 2 (find-index (rest xs) (first xs))))))))
