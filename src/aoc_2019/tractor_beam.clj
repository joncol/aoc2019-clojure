(ns aoc-2019.tractor-beam
  (:require [aoc-2019.util :as util]
            [clojure.string :as str]))

(defn parse-program [data]
  (->> (str/split data #",")
       (map util/parse-int)
       (map-indexed vector)
       (into (sorted-map))))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr rel-base mode]
  (case mode
    0 (or (get memory (get memory ptr)) 0)
    1 (or (get memory ptr) 0)
    2 (or (get memory (+ rel-base (get memory ptr))) 0)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) rel-base (mod param-modes 10))
     (param memory (+ ip 2) rel-base (mod (quot param-modes 10) 10))]))

(defn dest
  "Returns the destination for the instruction at `ip`."
  [memory ip rel-base]
  (let [at-ip (get memory ip)
        in-param-count (case (mod at-ip 100)
                         (1 2 7 8) 2
                         0)
        param-mode (quot at-ip (int (Math/pow 10 (+ 2 in-param-count))))
        out-param-ptr (+ ip in-param-count 1)]
    (case param-mode
      0 (get memory out-param-ptr)
      2 (+ rel-base (get memory out-param-ptr)))))

(defn run-program [inputs program]
  (loop [memory program
         ip 0
         rel-base 0
         inputs inputs
         outputs []]
    (let [op (mod (get memory ip) 100)
          [x y] (params memory ip rel-base)
          dest (delay (dest memory ip rel-base))]
      (case op
        (1 2) (let [op-fn (get {1 +', 2 *'} op)]
                (recur (assoc memory @dest (op-fn x y))
                       (+ ip 4)
                       rel-base
                       inputs
                       outputs))
        3 (recur (assoc memory @dest (first inputs))
                 (+ ip 2)
                 rel-base
                 (rest inputs)
                 outputs)
        4 (recur memory (+ ip 2) rel-base inputs (conj outputs x))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base inputs outputs))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* rel-base inputs outputs))
        7 (recur (assoc memory @dest (if (< x y) 1 0))
                 (+ ip 4)
                 rel-base
                 inputs
                 outputs)
        8 (recur (assoc memory @dest (if (= x y) 1 0))
                 (+ ip 4)
                 rel-base
                 inputs
                 outputs)
        9 (recur memory (+ ip 2) (+ rel-base x) inputs outputs)
        99 outputs))))

(defn grid [size]
  (->> (for [y (range size)]
         (for [x (range size)]
           [x y]))
       (apply concat)))

(defn draw-beam [program coords]
  (println (->> coords
                (mapcat #(run-program % program))
                (partition (int (Math/sqrt (count coords))))
                (map #(apply str %))
                (str/join "\n"))))

(defn part1 [data]
  (let [program (parse-program data)
        coords (grid 50)]
    (draw-beam program coords)
    (->> coords
         (mapcat #(run-program % program))
         (filter #(= 1 %))
         count)))

(defn part2 [data]
  (let [program (parse-program data)
        size 500
        coords (grid size)]
    (->> coords
         (mapcat #(run-program % program))
         (partition size)
         (map (partial drop-while zero?))
         (map #(take-while (partial = 1) %))
         (map count))))

#_(defn part2 [data]
    (let [program (parse-program data)
          size 500
          coords (grid size)]
      (->> coords
           (mapcat #(run-program % program))
           (partition size)
           (map (partial drop-while zero?))
           (map #(take-while (partial = 1) %))
           (map count))

      #_(map #(count (filter (partial = 1) %)))
      #_(map-indexed (fn [i x] [(+ offset i) x])))))
