(ns aoc-2019.space-image-format
  (:require [clojure.edn :as edn]
            [clojure.string :as str]))

(defn layer [image width height layer-index]
  (let [size (* width height)
        i (* size layer-index)]
    (subs image i (+ i size))))

(defn image-layers [data width height]
  (->> data
       (map (comp edn/read-string str))
       (partition (* width height))))

(defn part1 [data]
  (let [freqs (map frequencies (image-layers data 25 6))
        min-zeros-layer (apply min-key #(get % 0) freqs)]
    (* (get min-zeros-layer 1) (get min-zeros-layer 2))))

(defn pixel-values
  "Returns the pixel values for all layers at a specific position."
  [layers pos]
  (map (fn [l] (nth l pos)) layers))

(defn first-non-transparent [& pixels]
  (->> pixels
       (remove #(= 2 %))
       first))

(defn part2 [data]
  (let [width 25
        height 6
        layers (image-layers data width height)]
    (str "\n"
         (->> layers
              (apply map first-non-transparent)
              (map #(get {0 \space, 1 \#} %))
              (partition width)
              (map str/join)
              (str/join "\n")))))
