(ns aoc-2019.monitoring-station
  (:require [clojure.string :as str]))

(defn parse-map [data]
  (->> data
       str/split-lines
       (reduce (fn [[asteroids y] row]
                 (let [asteroids*
                       (first (reduce (fn [[asteroids x] ch]
                                        (if (= \# ch)
                                          [(conj asteroids [x y]) (inc x)]
                                          [asteroids (inc x)]))
                                      [asteroids 0]
                                      row))]
                   [asteroids* (inc y)]))
               [[] 0])
       first))

(defn gcd [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defn in-line-of-sight-count [asteroids [xp yp]]
  (let [slopes (for [[xa ya] asteroids
                     :when (not (and (= xa xp) (= ya yp)))]
                 (let [xd (- xa xp)
                       yd (- ya yp)
                       d (Math/abs (gcd xd yd))]
                   [(quot xd d) (quot yd d)]))]
    (count (group-by identity slopes))))

(defn part1 [data]
  (let [asteroids (parse-map data)
        counts (map (partial in-line-of-sight-count asteroids) asteroids)]
    (apply max counts)))

(defn distance-squared [[x1 y1] [x2 y2]]
  (+ (Math/pow (- x2 x1) 2)
     (Math/pow (- y2 y1) 2)))

(defn asteroid-angles [asteroids [xp yp]]
  (let [asteroids-by-angle
        (->> (for [[xa ya] asteroids
                   :when (not (and (= xa xp) (= ya yp)))]
               (let [xd (- xa xp)
                     yd (- ya yp)
                     angle (mod (* (/ 180 Math/PI)
                                   (- (/ Math/PI 2) (Math/atan2 (- yd) xd)))
                                360)]
                 [angle [xa ya]]))
             (reduce (fn [acc [k v]] (update acc k conj v)) {}))]
    (->> asteroids-by-angle
         (map (fn [[angle asteroids]]
                [angle (sort-by (partial distance-squared [xp yp]) asteroids)]))
         (into (sorted-map)))))

(defn asteroids-order [asteroids-by-angle]
  (apply concat (reduce
                 (fn [acc e]
                   (reduce-kv
                    (fn [acc2 i2 e2]
                      (assoc acc2 i2 ((fnil conj []) (get acc2 i2) e2)))
                    acc
                    (vec e)))
                 []
                 asteroids-by-angle)))

(defn part2 [data]
  (let [asteroids (parse-map data)
        counts (map (fn [a] [a (in-line-of-sight-count asteroids a)])
                    asteroids)
        from-asteroid (first (apply max-key second counts))
        asteroids-ordered (vals (asteroid-angles asteroids from-asteroid))
        [x y] (nth (asteroids-order asteroids-ordered) (dec 200))]
    (+ (* 100 x) y)))
