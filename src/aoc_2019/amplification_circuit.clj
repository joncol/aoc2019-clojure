(ns aoc-2019.amplification-circuit
  (:require [aoc-2019.util :as util]
            [clojure.core.async :as async]
            [clojure.string :as str]))

(defn parse-data [data]
  (mapv util/parse-int (str/split data #",")))

(defn param
  "Returns a single parameter at `memory` position given by `ptr`."
  [memory ptr mode]
  (case mode
    0 (get memory (get memory ptr))
    1 (get memory ptr)))

(defn params
  "Returns the parameters for the instruction at `ip`."
  [memory ip]
  (let [at-ip (nth memory ip)
        param-modes (quot at-ip 100)]
    [(param memory (inc ip) (mod param-modes 10))
     (param memory (+ ip 2) (quot param-modes 10))]))

(defn run-program! [memory stop-chs io-chs amp-index]
  (loop [memory memory
         ip 0
         latest-output nil]
    (let [op (mod (nth memory ip) 100)
          [x y] (params memory ip)]
      (case op
        (1 2) (let [dest (nth memory (+ ip 3))
                    op-fn (get {1 +, 2 *} op)]
                (recur (assoc memory dest (op-fn x y)) (+ ip 4) latest-output))
        3 (recur (assoc memory
                        (nth memory (inc ip))
                        (async/<!! (nth io-chs amp-index)))
                 (+ ip 2)
                 latest-output)
        4 (do (async/>!! (nth io-chs (mod (inc amp-index) (count io-chs))) x)
              (recur memory (+ ip 2) x))
        5 (let [ip* (if-not (zero? x) y (+ ip 3))]
            (recur memory ip* latest-output))
        6 (let [ip* (if (zero? x) y (+ ip 3))]
            (recur memory ip* latest-output))
        7 (let [dest (nth memory (+ ip 3))]
            (recur (assoc memory dest (if (< x y) 1 0)) (+ ip 4) latest-output))
        8 (let [dest (nth memory (+ ip 3))]
            (recur (assoc memory dest (if (= x y) 1 0)) (+ ip 4) latest-output))
        99 (async/>!! (nth stop-chs amp-index) latest-output)))))

(defn rotations [a-seq]
  (let [a-vec (vec a-seq)]
    (for [i (range (count a-vec))]
      (concat (subvec a-vec i) (subvec a-vec 0 i)))))

(defn permutations [a-set]
  (if (empty? a-set)
    (list ())
    (mapcat (fn [[x & xs]] (map #(cons x %) (permutations xs)))
            (rotations a-set))))

(defn max-thruster-signal [phase-setting-digits-set data]
  (let [program (parse-data data)
        signals (for [phase-settings (permutations phase-setting-digits-set)]
                  (let [stop-chs (repeatedly 5 #(async/chan 1))
                        io-chs (repeatedly 5 #(async/chan 2))]
                    (dotimes [i 5]
                      (async/>!! (nth io-chs i) (nth phase-settings i)))
                    (async/>!! (first io-chs) 0)
                    (dotimes [i 5]
                      (async/thread (run-program! program stop-chs io-chs i)))
                    (last (async/<!! (async/map vector stop-chs)))))]
    (apply max signals)))

(def part1 (partial max-thruster-signal (range 5)))

(def part2 (partial max-thruster-signal (range 5 10)))
