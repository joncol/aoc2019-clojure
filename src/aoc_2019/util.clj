(ns aoc-2019.util
  (:require [clojure.string :as str]))

(defn parse-int [s]
  (->> s
       (take-while #(or (Character/isDigit %)
                        (get (set "-+") %)))
       str/join
       Integer/parseInt))

(defn parse-big-int [s]
  (->> s
       (take-while #(or (Character/isDigit %)
                        (get (set "-+") %)))
       str/join
       BigInteger.))

(defn rotate
  "Rotates a sequence `s` to the left `n` positions. If `n` is negative, rotates
  to the right."
  [n s]
  (let [[front back] (split-at (mod n (count s)) s)]
    (concat back front)))

(defn print-array
  "Prints the elements of the array `a`."
  [a]
  (dotimes [i (count a)]
    (print (aget a i))
    (when (< i (dec (count a)))
      (print ", ")))
  (println))

(defn aswap!
  "Swaps the elements at positions `i` and `j` of the array `a`."
  [^"[I" a ^long i ^long j]
  (let [tmp ^int (aget a i)]
    (aset a i (aget a j))
    (aset a j tmp)
    a))

(defn areverse!
  "Reverses (part of) the array `a` in-place."
  ([^"[I" a] (areverse! a 0 (dec (count a))))
  ([^"[I" a ^long l ^long r]
   (when (< l r)
     (let [tmp ^int (aget a l)]
       (aset a l (aget a r))
       (aset a r tmp)
       (recur a (inc l) (dec r))))))

(defn arotate!
  "Rotates a sub-array (of length `cnt`) of `a` to the left `n` positions. If
  `n` is negative, rotates to the right. Does the rotation in-place.
  `cnt` defaults to the length of `a`"
  ([a ^long n] (arotate! a n (count a)))
  ([a ^long n ^long cnt]
   (let [n* (mod n cnt)
         l (java.util.Arrays/copyOfRange a 0 n*)]
     (System/arraycopy a n* a 0 (unchecked-subtract-int cnt n*))
     (System/arraycopy l 0 a (unchecked-subtract-int cnt n*) n*)
     a)))

(defn find-values
  "Find indices of value in sequence."
  [coll value]
  (map first
       (filter #(= (second %) value)
               (map-indexed vector coll))))

(defn afind-value
  "Find first index of `needle` in `a`."
  [^"[I" a ^long needle]
  (loop [i 0]
    (if (= needle (aget a i))
      i
      (when (< (inc i) (count a))
        (recur (inc i))))))

(defn- signum
  "Not a true signum function, since it never returns 0."
  [n]
  (cond
    (neg? n)  -1
    :else     1))

(defn- int-sqrt [n]
  (let [r  (Math/sqrt (float n))
        r* (int r)]
    (when (= (float (* r* r*)) (* r r))
      r*)))

(defn solve-int-quadratic
  "Solves equations of the form ax^2 + by + c = 0, using Viete's formula to
  minimize numerical instability. Only gives integer roots, and assumes `a`, `b`
  and `c` are integers. Returns true if there are infinite roots for the
  equation."
  [a b c]
  (cond
    (and (zero? a) (zero? b) (zero? c)) true ; infinite solutions
    (and (zero? a) (or (zero? b) (not (zero? (mod c b))))) []
    (zero? a) [(quot (- c) b)]
    :else (let [disc (- (* b b) (* 4 a c))
                root (int-sqrt disc)]
            (cond
              (neg? disc)  []
              (nil? root)  []
              (zero? disc) (if (even? b) [(- (quot b (* 2 a)))] [])
              :else (let [q (* -0.5 (+ b (* (signum b) root)))
                          [x1 x2] [(/ q a) (/ c q)]]
                      (->> (if (< x1 x2)
                             [x1 x2]
                             [x2 x1])
                           (filter #(= (Math/ceil %) (float %)))
                           (map int)
                           vec))))))
