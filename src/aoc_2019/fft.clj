(ns aoc-2019.fft
  (:require [clojure.string :as str]))

(def repeat-pattern
  (memoize (fn [xs n]
             (mapcat #(repeat n %) xs))))

(defn fft [^String x]
  (let [base-pattern [0 1 0 -1]
        input-digits (map #(- (int %) (int \0)) x)
        n (count input-digits)]
    (->> (for [oi (range n)]
           (let [pattern (rest (cycle (repeat-pattern base-pattern (inc oi))))]
             (-> (reduce + (map * input-digits pattern))
                 Math/abs
                 (mod 10))))
         (apply str))))

(defn part1 [data]
  (subs (nth (iterate fft (str/trim data)) 100) 0 8))

(defn fft* [^String x]
  (let [offset (Integer/parseUnsignedInt (subs x 0 7))
        input-digits (map #(- (int %) (int \0)) x)
        tail (->> input-digits
                  (repeat 10000)
                  (apply concat)
                  (drop offset)
                  reverse)
        fft-step-fn #(reductions (fn [carry d] (mod (+ carry d) 10)) %)]
    (apply str (-> (iterate fft-step-fn tail)
                   (nth 100)
                   reverse))))

(defn part2 [data]
  (subs (fft* (str/trim data)) 0 8))
