(defproject aoc-2019 "0.1.0-SNAPSHOT"
  :description "Advent of Code 2019"
  :dependencies [[clojure-lanterna "0.9.7"]
                 [instaparse "1.4.10"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "0.6.532"]
                 [org.clojure/core.cache "0.8.2"]]
  :main ^:skip-aot aoc-2019.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
